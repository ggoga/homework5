package com.example.homework5

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.room.Room
import com.example.homework5.model.UserData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var db: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        findViewById<Button>(R.id.btn_submit).setOnClickListener(this)

        db = Room.databaseBuilder(
            applicationContext,
            AppDatabase::class.java, "app-db"
        ).build()
    }

    private fun getCurrentData(): UserData {
        val swimDist: Double =
            findViewById<EditText?>(R.id.swim_dist).text.toString().toDouble()

        val runDist: Double = findViewById<EditText?>(R.id.run_dist).text.toString().toDouble()

        val caloriesTaken: Double =
            findViewById<EditText?>(R.id.calories_taken).text.toString().toDouble()

        return UserData(
            distanceRan = runDist,
            distanceSwam = swimDist,
            caloriesTaken = caloriesTaken
        )
    }

    private suspend fun getAverageData(): UserData {
        val dao = db.userDataDao()

        val avgCaloriesTaken: Double = dao.getAverageCaloriesTaken()
        val avgRunDist: Double = dao.getAverageDistanceRan()
        val avgSwimDist: Double = dao.getAverageDistanceSwam()

        return UserData(
            distanceRan = avgRunDist,
            distanceSwam = avgSwimDist,
            caloriesTaken = avgCaloriesTaken
        )
    }

    override fun onClick(view: View?) {
        if (view is Button) {
            val dao = db.userDataDao()

            lifecycleScope.launch {
                withContext(Dispatchers.Default) { dao.insert(getCurrentData()) }

                val totalDistanceRanText = "Total distance ran: ${dao.getTotalDistanceRan()}\n"
                findViewById<TextView>(R.id.total_distance_display).text = totalDistanceRanText

                val averageData: UserData =
                    withContext(Dispatchers.Default) { getAverageData() }

                val averageDataText = "Avg distance ran: ${averageData.distanceRan}\n" +
                        "Avg distance swam: ${averageData.distanceSwam}\n" +
                        "Avg calories taken: ${averageData.caloriesTaken}"
                findViewById<TextView>(R.id.average_data_display).text = averageDataText
            }
        }
    }
}